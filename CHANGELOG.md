### Version 0.9.0 Tag: release-1 (2018-08-14)
 * initial release after fork

### Version: 0.9.1 Tag: release-2 (2018-08-23) ###
 * Fix: remove expanded items in the library view after resume
 * Fix: check device screen size and quit when too small

### Version: 0.9.2 Tag: release-3 (2018-09-15) ###
 * Fix: removed art providers requiring API keys of the M.A.L.P project
 * Fix: many fixes for art work loading via HTTP
