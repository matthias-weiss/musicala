# Contributors

 - Matthias Weiss - musicala user interface rework

 - Hendrik Borghorst - M.A.L.P Main developer
 - Frederik Lütkes - M.A.L.P Shared Odyssey Code (https://github.com/gateship-one/odyssey)
 - naofum (Github user) - M.A.L.P Japanese translation
 - Guy de Mölkky (Github user) - M.A.L.P French translation
 - Víctor García - M.A.L.P Spanish translation
 - Minho Park - M.A.L.P Korean translation

 and more M.A.L.P [Contributors](https://github.com/gateship-one/malp/graphs/contributors)
