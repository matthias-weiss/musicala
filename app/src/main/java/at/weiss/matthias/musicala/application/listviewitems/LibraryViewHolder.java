package at.weiss.matthias.musicala.application.listviewitems;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import at.weiss.matthias.musicala.R;
import at.weiss.matthias.musicala.application.artworkdatabase.ArtworkManager;
import at.weiss.matthias.musicala.application.artworkdatabase.ImageNotFoundException;
import at.weiss.matthias.musicala.application.utils.AsyncLoader;
import at.weiss.matthias.musicala.application.utils.ThemeUtils;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDAlbum;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDArtist;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDTrack;

public class LibraryViewHolder extends RecyclerView.ViewHolder
        implements CoverLoadable,
                   ArtworkManager.onNewAlbumImageListener,
                   ArtworkManager.onNewArtistImageListener {

    private static final String TAG = LibraryViewHolder.class.getSimpleName();

    public ConstraintLayout mItemContainer;
    public ImageView mImage;
    public TextView  mPrefixText;
    public TextView  mMainText;
    public TextView  mPostfixText;

    public ImageButton mPlayReplace;
    public ImageButton mPlayInsertAfterCursor;
    public ImageButton mPlayAppend;

    public Activity       mActivity;
    public AsyncLoader    mLoaderTask;
    public LibraryItem    mItem;
    public ArtworkManager mArtworkManager;

    public LibraryViewHolder(ConstraintLayout itemContainer, int viewType, Context context, ArtworkManager artworkManager) {
        super(itemContainer);

        mActivity = (Activity) context;
        mArtworkManager = artworkManager;
        mArtworkManager.registerOnNewAlbumImageListener(this);
        mArtworkManager.registerOnNewArtistImageListener(this);

        mItemContainer = itemContainer.findViewById(R.id.recycler_item_library_item_container);

        mImage       = (ImageView) mItemContainer.findViewById(R.id.recycler_item_library_image);
        mPrefixText  = (TextView)  mItemContainer.findViewById(R.id.recycler_item_library_prefix_text);
        mMainText    = (TextView)  mItemContainer.findViewById(R.id.recycler_item_library_main_text);
        mPostfixText = (TextView)  mItemContainer.findViewById(R.id.recycler_item_library_postfix_text);

        mPlayReplace           = (ImageButton) mItemContainer.findViewById(R.id.recycler_item_library_play_replace);
        mPlayInsertAfterCursor = (ImageButton) mItemContainer.findViewById(R.id.recycler_item_library_play_insert_after_cursor);
        mPlayAppend            = (ImageButton) mItemContainer.findViewById(R.id.recycler_item_library_play_append);

        mLoaderTask = null;

        if (viewType == MPDArtist.VIEW_TYPE) {
            mPostfixText.setVisibility(View.GONE);
        }

        if (viewType != MPDTrack.VIEW_TYPE) {
            mPrefixText.setVisibility(View.GONE);
        }
        switch (viewType) {
            case MPDArtist.VIEW_TYPE:
                setItemColors(MPDArtist.VIEW_TYPE, false);
                mPrefixText.setVisibility(View.GONE);
                mPostfixText.setVisibility(View.GONE);
                break;
            case MPDAlbum.VIEW_TYPE:
                setItemColors(MPDAlbum.VIEW_TYPE, false);
                mPrefixText.setVisibility(View.GONE);
                mPostfixText.setVisibility(View.VISIBLE);
                break;
            case MPDTrack.VIEW_TYPE:
                setItemColors(MPDTrack.VIEW_TYPE, false);
                mImage.setVisibility(View.GONE);
                mPrefixText.setVisibility(View.VISIBLE);
                mPostfixText.setVisibility(View.VISIBLE);
                break;
        }

        mPlayReplace.setVisibility(View.GONE);
        mPlayInsertAfterCursor.setVisibility(View.GONE);
        mPlayAppend.setVisibility(View.GONE);
    }

    public void setItemColors(int viewType, boolean expanded){
        int backgroundColor = 0;
        int textColor = 0;
        Typeface font = null;

        switch(viewType) {
            case MPDArtist.VIEW_TYPE:
                if (expanded) {
                    backgroundColor = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_accent);
                    textColor       = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_on_accent);
                } else {
                    backgroundColor = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_surface);
                    textColor       = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_on_surface);
                }
                font            = Typeface.create("serif", Typeface.NORMAL);
                break;
            case MPDAlbum.VIEW_TYPE:
                backgroundColor = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_accent);
                textColor       = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_on_accent);
                font            = Typeface.create("serif-medium", Typeface.ITALIC);
                break;
            case MPDTrack.VIEW_TYPE:
                backgroundColor = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_accent);
                textColor       = ThemeUtils.getThemeColor(mActivity, R.attr.musicala_color_on_accent);
                font            = Typeface.create("monospace", Typeface.BOLD);
                break;
        }

        mItemContainer.setBackgroundColor(backgroundColor);

        mImage.setBackgroundColor(backgroundColor);

        mPrefixText.setTextColor(textColor);
        mPrefixText.setTypeface(font);

        mMainText.setTextColor(textColor);
        mMainText.setTypeface(font);

        mPostfixText.setTextColor(textColor);
        mPostfixText.setTypeface(font);
    }

    public void loadImage(LibraryItem listItem) {

        mItem = listItem;

        Bitmap image = null;
        // Check if model item is artist or album
        if (listItem.getViewType() == MPDArtist.VIEW_TYPE) {
            MPDArtist artist = (MPDArtist) listItem;
            try {
                // Check if image is available. If it is not yet fetched it will throw an exception
                // If it was already searched for and not found, this will be null.
                image = mArtworkManager.getArtistImage(artist, 0, 0, false);
            } catch (ImageNotFoundException e) {
                // Check if fetching for this item is already ongoing
                if (!artist.getFetching()) {
                    // If not set it as ongoing and request the image fetch.
                    mArtworkManager.fetchArtistImage(artist);
                    artist.setFetching(true);
                }
            }
        } else if (listItem.getViewType() == MPDAlbum.VIEW_TYPE) {
            MPDAlbum album = (MPDAlbum)listItem;
            try {
                // Check if image is available. If it is not yet fetched it will throw an exception.
                // If it was already searched for and not found, this will be null.
                image = mArtworkManager.getAlbumImage(album, 0, 0, false);
            } catch (ImageNotFoundException e) {
                // Check if fetching for this item is already ongoing
                if (!album.getFetching()) {
                    // If not set it as ongoing and request the image fetch.
                    mArtworkManager.fetchAlbumImage(album);
                    album.setFetching(true);
                }
            }
        }
        setImage(image);
    }

    @Override
    public void newAlbumImage(MPDAlbum album) {
        if (mItem.getViewType() == MPDAlbum.VIEW_TYPE && mItem.getMainText().equals(album.getMainText())) {
            try {
                Log.v(TAG, "newAlbumImage: callback for: " + album.getMainText());
                Bitmap image = mArtworkManager.getAlbumImage(album, 0, 0, false);
                setImage(image);
            } catch (ImageNotFoundException e) {
                Log.v(TAG, "Giving up getting image for: " + album.getMainText());
            }
        }
    }

    @Override
    public void newArtistImage(MPDArtist artist) {
        if (mItem.getViewType() == MPDArtist.VIEW_TYPE && mItem.getMainText().equals(artist.getMainText())) {
            try {
                Log.v(TAG, "newArtistImage: callback for: " + artist.getMainText());
                Bitmap image = mArtworkManager.getArtistImage(artist, 0, 0, false);
                setImage(image);
            } catch (ImageNotFoundException e) {
                Log.v(TAG, "Giving up getting image for: " + artist.getMainText());
            }
        }
    }

    public void setImage(Bitmap image) {

        if (mActivity != null && image != null) {
            mActivity.runOnUiThread(() -> {
                Log.v(TAG, "showing bitmap for: " + mMainText.getText());
                mImage.setImageBitmap(image);
            });
        }
    }
}