/*
 *  Copyright (C) 2018 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/matthias-weiss/musicala/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package at.weiss.matthias.musicala.application.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Pair;


import at.weiss.matthias.musicala.application.adapters.ScrollSpeedAdapter;
import at.weiss.matthias.musicala.application.artworkdatabase.ArtworkManager;
import at.weiss.matthias.musicala.application.artworkdatabase.ImageNotFoundException;
import at.weiss.matthias.musicala.application.listviewitems.CoverLoadable;
import at.weiss.matthias.musicala.application.listviewitems.LibraryItem;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDAlbum;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDArtist;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDGenericItem;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDTrack;

/*
 * Loaderclass for covers
 */
public class AsyncLoader extends AsyncTask<AsyncLoader.LoaderItem, Void, Bitmap> {
    
    private static final String TAG = AsyncLoader.class.getSimpleName();
    private LoaderItem mLoaderItem;

    /**
     * Wrapper class for covers
     */
    public static class LoaderItem {
        public Pair<Integer, Integer> imageDimension;
        public CoverLoadable coverLoadable;
        public ArtworkManager artworkManager;
        public LibraryItem modelItem;
        public ScrollSpeedAdapter mAdapter;
    }

    /**
     * Asynchronous task in parallel to the GUI thread.
     * @param params Input parameter containing all the necessary informaton to fetch the image.
     * @return Bitmap loaded from the database.
     */
    @Override
    protected Bitmap doInBackground(LoaderItem... params) {
        mLoaderItem = params[0];
        Bitmap image = null;
        // Check if model item is artist or album
        if (mLoaderItem.modelItem instanceof MPDArtist) {
            MPDArtist artist = (MPDArtist)mLoaderItem.modelItem;
            try {
                // Check if image is available. If it is not yet fetched it will throw an exception
                // If it was already searched for and not found, this will be null.
                image = mLoaderItem.artworkManager.getArtistImage(artist, mLoaderItem.imageDimension.first, mLoaderItem.imageDimension.second, false);
            } catch (ImageNotFoundException e) {
                // Check if fetching for this item is already ongoing
                if (!artist.getFetching()) {
                    // If not set it as ongoing and request the image fetch.
                    mLoaderItem.artworkManager.fetchArtistImage(artist);
                    artist.setFetching(true);
                }
            }
        } else if (mLoaderItem.modelItem instanceof MPDAlbum) {
            MPDAlbum album = (MPDAlbum)mLoaderItem.modelItem;
            try {
                // Check if image is available. If it is not yet fetched it will throw an exception.
                // If it was already searched for and not found, this will be null.
                image = mLoaderItem.artworkManager.getAlbumImage(album, mLoaderItem.imageDimension.first, mLoaderItem.imageDimension.second, false);
            } catch (ImageNotFoundException e) {
                // Check if fetching for this item is already ongoing
                if (!album.getFetching()) {
                    // If not set it as ongoing and request the image fetch.
                    mLoaderItem.artworkManager.fetchAlbumImage(album);
                    album.setFetching(true);
                }
            }
        } else if (mLoaderItem.modelItem instanceof MPDTrack) {
            MPDTrack track = (MPDTrack)mLoaderItem.modelItem;
            try {
                // Check if image is available. If it is not yet fetched it will throw an exception.
                // If it was already searched for and not found, this will be null.
                image = mLoaderItem.artworkManager.getAlbumImageForTrack(track, mLoaderItem.imageDimension.first, mLoaderItem.imageDimension.second, false);
            } catch (ImageNotFoundException e) {
                // Check if fetching for this item is already ongoing
                if (!track.getFetching()) {
                    // If not set it as ongoing and request the image fetch.
                    mLoaderItem.artworkManager.fetchAlbumImage(track);
                    track.setFetching(true);
                }
            }
        }
        return image;
    }


    /**
     * Called when the asynchronous task finishes. This is called inside the GUI context.
     * @param result Bitmap that was loaded.
     */
    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);

        if ( null != result ) {
            // Set the newly loaded image to the view.
            mLoaderItem.coverLoadable.setImage(result);
        }
    }
}