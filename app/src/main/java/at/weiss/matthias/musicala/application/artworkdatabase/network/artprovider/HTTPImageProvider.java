/*
 *  Copyright (C) 2018 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/matthias-weiss/musicala/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package at.weiss.matthias.musicala.application.artworkdatabase.network.artprovider;


import android.content.Context;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import java.net.URI;
import java.net.URISyntaxException;

import at.weiss.matthias.musicala.application.artworkdatabase.network.requests.AlbumImageByteRequest;
import at.weiss.matthias.musicala.application.artworkdatabase.network.requests.ArtistImageByteRequest;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.AlbumFetchError;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.AlbumImageResponse;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.ArtistFetchError;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.ArtistImageResponse;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDAlbum;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDArtist;

public class HTTPImageProvider
        implements AlbumImageProvider,
                   ArtistImageProvider {
    private static final String TAG = HTTPImageProvider.class.getSimpleName();

    /**
     * Filename combinations used if only a directory is specified
     */
    private static final String[] COVER_FILENAMES = {"cover","folder","Cover","Folder"};


    private static final String[] ARTIST_ART_FILENAMES = {"cover","folder","artist", "Cover","Folder", "Artist"};
    /**
     * File extensions tried for all filenames
     */
    private static final String[] ART_FILEEXTENSIONS = {"png","jpg","jpeg","PNG","JPG","JPEG"};

    /**
     * Singleton instance
     */
    private static HTTPImageProvider mInstance;

    /**
     * URI used for downloading
     */
    private static URI mURI;

    /**
     * {@link RequestQueue} used for downloading images. Separate queue for this provider
     * because no request limitations are necessary
     */
    private RequestQueue mRequestQueue;


    private HTTPImageProvider(Context context) {
        // Don't use MALPRequestQueue because we do not need to limit the load on the local server
        Network network = new BasicNetwork(new HurlStack());
        // 10MB disk cache
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024 * 10);

        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
    }

    public static synchronized HTTPImageProvider getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new HTTPImageProvider(context);
        }

        return mInstance;
    }

    public void setURL(String url) {
        if (url != null && url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        try {
            mURI = new URI(url);
        } catch(Exception e) {
            Log.v(TAG, "Exception: " + e.getMessage());
            mURI = null;
        }
    }

    public boolean getActive() {
        if (mURI == null) {
            return false;
        }
        return true;
    }

    private String getEncodedURL(String artist_or_album) {

        URI uri = null;
        try {
            uri = new URI(mURI.getScheme(), null, mURI.getHost(), mURI.getPort(), mURI.getPath() + "/" + artist_or_album, null, null);
        } catch (URISyntaxException e) {
            Log.v(TAG, "URISyntaxException: " + e.getMessage());
        }

        Log.v(TAG, "Uri encode result for " + artist_or_album + ": " + uri.toASCIIString());

        return uri.toASCIIString();
    }

    @Override
    public void fetchArtistImage(final MPDArtist artist, Response.Listener<ArtistImageResponse> listener, final ArtistFetchError errorListener) {

        String url = getEncodedURL(artist.getArtistName() + "/");
        Request<ArtistImageResponse> byteResponse;

        final HTTPMultiRequest multiRequest = new HTTPMultiRequest(artist, errorListener);
        // Directory check all pre-defined files
        for(String filename : ARTIST_ART_FILENAMES) {
            for (String fileextension: ART_FILEEXTENSIONS) {
                String fileURL = url + filename + '.' + fileextension;
                byteResponse = new ArtistImageByteRequest(fileURL, artist, listener, multiRequest::increaseFailure);
                mRequestQueue.add(byteResponse);
            }
        }
    }
    @Override
    public void fetchAlbumImage(final MPDAlbum album, Response.Listener<AlbumImageResponse> listener, final AlbumFetchError errorListener) {

        String url = getEncodedURL(album.getArtistName() + "/" + album.getName() + "/");
        Request<AlbumImageResponse> byteResponse = null;

        final HTTPMultiRequest multiRequest = new HTTPMultiRequest(album, errorListener);
        // Directory check all pre-defined files
        for(String filename : COVER_FILENAMES) {
            for (String fileextension: ART_FILEEXTENSIONS) {
                String fileURL = url + filename + '.' + fileextension;
                byteResponse = new AlbumImageByteRequest(fileURL, album, listener, multiRequest::increaseFailure);
                mRequestQueue.add(byteResponse);
            }
        }
    }

    private class HTTPMultiRequest {
        private int mFailureCount;
        private AlbumFetchError mAlbumErrorListener;
        private ArtistFetchError mArtistErrorListener;
        private MPDAlbum mAlbum;
        private MPDArtist mArtist;

        public HTTPMultiRequest(MPDAlbum album, AlbumFetchError errorListener) {
            mAlbum = album;
            mAlbumErrorListener = errorListener;
            mArtist = null;
            mArtistErrorListener = null;
        }

        public HTTPMultiRequest(MPDArtist artist, ArtistFetchError errorListener) {
            mArtist = artist;
            mArtistErrorListener = errorListener;
            mAlbum = null;
            mAlbumErrorListener = null;
        }

        public synchronized void increaseFailure(VolleyError error) {
            mFailureCount++;
            if (mAlbumErrorListener != null && mAlbum != null) {
                if (mFailureCount == COVER_FILENAMES.length * ART_FILEEXTENSIONS.length) {
                    mAlbumErrorListener.fetchVolleyError(mAlbum, error);
                }
            } else if (mArtistErrorListener != null && mArtist != null) {
                if (mFailureCount == ARTIST_ART_FILENAMES.length * ART_FILEEXTENSIONS.length) {
                    mArtistErrorListener.fetchVolleyError(mArtist, error);
                }
            }
        }
    }
}
